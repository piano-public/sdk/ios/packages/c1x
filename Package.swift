// swift-tools-version:5.10

import PackageDescription

let package = Package(
    name: "PianoC1X",
    platforms: [
        .iOS(.v12),
        .tvOS(.v12)
    ],
    products: [
       .library(
           name: "PianoC1X",
           targets: ["PianoC1X"]
       )
    ],
    dependencies: [
        .package(url: "https://github.com/cXense/cxense-spm", .upToNextMinor(from: "1.10.1")),
        .package(url: "https://gitlab.com/piano-public/sdk/ios/package", .upToNextMinor(from: "2.8.6"))
    ],
    targets: [
        .target(
            name: "PianoC1X",
            dependencies: [
                .product(name: "CxenseSDK", package: "cxense-spm"),
                .product(name: "PianoComposer", package: "package"),
                .product(name: "PianoTemplate", package: "package")
            ],
            path: "Sources"
        ),
        .testTarget(
            name: "PianoC1XTests",
            dependencies: ["PianoC1X"],
            path: "Tests"
        ),
    ]
)
