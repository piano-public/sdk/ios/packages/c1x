Pod::Spec.new do |s|
  s.name         = 'PianoC1X'
  s.version      = '2.8.6'
  s.swift_version = '5.10'
  s.summary      = 'Enables iOS apps to use C1X integration by Piano.io'
  s.homepage     = 'https://gitlab.com/piano-public/sdk/ios/packages/c1x'
  s.license      = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
  s.author       = 'Piano Inc.'
  s.platform     = :ios
  s.ios.deployment_target = '12.0'
  s.source       = { :git => 'https://gitlab.com/piano-public/sdk/ios/packages/c1x.git', :tag => "#{s.version}" }
  s.resource_bundle = {
      "PianoSDK_C1X" => ["Sources/Resources/*"]
  }
  s.source_files = 'Sources/**/*.swift', 'Sources/**/*.h'
  s.static_framework = true
  s.dependency 'PianoComposer', "~> #{s.version}"
  s.dependency 'PianoTemplate', "~> #{s.version}"
  s.dependency 'CxenseSDK', '~> 1.10.1'
end
