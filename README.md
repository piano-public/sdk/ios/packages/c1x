# Piano Composer integration with Cxense for iOS (C1X)

[![Version](https://img.shields.io/cocoapods/v/PianoC1X.svg?style=flat)](http://cocoapods.org/pods/PianoC1X)
[![Platform](https://img.shields.io/cocoapods/p/PianoC1X.svg?style=flat)](http://cocoapods.org/pods/PianoC1X)
[![License](https://img.shields.io/cocoapods/l/PianoC1X.svg?style=flat)](http://cocoapods.org/pods/PianoC1X)

> Important: Don't send Cxense PV events from screens with Composer

## Installation

### [CocoaPods](https://cocoapods.org/)

Add the following lines to your `Podfile`.

```
use_frameworks!

pod 'PianoC1X', '~>2.8.6'
```

Then run `pod install`. For details of the installation and usage of CocoaPods, visit [official web site](https://cocoapods.org/).

### [Swift Package Manager](https://developer.apple.com/documentation/swift_packages/adding_package_dependencies_to_your_app)
Add the component `PianoC1X` from the repository:

**URL:** https://gitlab.com/piano-public/sdk/ios/packages/c1x

**Version:** 2.8.6

## Import

```swift
import PianoC1X
```

## Configuration

```swift
/// Initialize Cxense SDK
do {
    try Cxense.initialize(withConfiguration: ...)
} catch {
    ...
}
```

## Integration

```swift
let configuration = PianoC1XConfiguration(siteId: "<SITE_ID>")
...
PianoC1X.enable(configuration: configuration)
```

## Usage
If integration is enabled, Composer will automatically send PV event to Cxense after execution.
> Important: You have to fill in url parameter for Composer:
>```swift
> PianoComposer(aid: ..., endpoint: ...)
>     ...
>     .url("https://example.com")
>     ...
>```

## Disable C1X
```swift
PianoC1X.disable()
```
